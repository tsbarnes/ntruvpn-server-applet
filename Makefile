PREFIX:=/usr/local

all:

install:
	install -g root -m 755 -o root -t $(DESTDIR)$(PREFIX)/bin ntruvpn-server-applet
	install -g root -m 755 -o root -t $(DESTDIR)$(PREFIX)/sbin ntruvpn-server-controller
	install -g root -m 644 -o root -t $(DESTDIR)$(PREFIX)/share/applications data/applications/ntruvpn-server-applet.desktop
	install -g root -m 644 -o root -t $(DESTDIR)/etc/xdg/autostart data/applications/ntruvpn-server-applet.desktop
	install -g root -m 644 -o root -t $(DESTDIR)$(PREFIX)/share/icons data/icons/ntruvpn.svg data/icons/ntruvpn-off.svg
